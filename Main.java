import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.lang.Math;

public class Main {
    public static void main(String[] args) {
        List<List<String>> records = readData("thyroid.csv");
        List<List<Double>> recordsDouble = stringToDouble(records);

        // Tes print isi data array string
        // for(List<String> recordsLine : records) {
        // for(String cellLine: recordsLine) {
        // System.out.println(cellLine);
        // }
        // }

        // Tes print isi data array double
        // for (List<Double> recordsDoubleLine : recordsDouble) {
        // for (Double cellIntLine : recordsDoubleLine) {
        // System.out.print(cellIntLine + "\t");
        // }
        // System.out.println();
        // }

        // Split data training 80% dan data testing 20%

        // Tes print isi data training yg udah dinormalisasi
        // int i = 1;
        // for(List<Double>line : dataTraining) {
        // System.out.print(i + " => ");
        // for(Double cell : line) {
        // System.out.print(cell + "\t");
        // }
        // i++;
        // System.out.println();
        // }

        // Tes print isi data testing yg udah dinormalisasi
        // int i = 0;
        // for(List<Double>line : dataTesting) {
        // System.out.print(i + " => ");
        // for(Double cell : line) {
        // System.out.print(cell + "\t");
        // }
        // i++;
        // System.out.println();
        // }

        // Klasifikasi
        // int k = 5;
        int k[] = { 1, 5, 10 };

        System.out.println("Error result (in %)");
        System.out.println("k\tH\tK\tL");
        for (int cell : k) {
            // ambil kembalian error holdout
            double holdoutError = knnWithHoldout(recordsDouble, cell, 0.8);

            // ambil kembalian error kfold
            double kFoldError = knnWithKFold(recordsDouble, cell, 0.2);

            // ambil kembalian error loo
            double looError = knnWithLOO(recordsDouble, cell);

            System.out.println(cell + "\t" + String.format("%,.2f", holdoutError) + "\t"
                    + String.format("%,.2f", kFoldError) + "\t" + String.format("%,.2f", looError));
        }
    }

    // Baca data thyroid.csv dan menyimpan ke variabel penampung 'records'
    static List<List<String>> readData(String url) {
        final String COMMA_DELIMITER = ",";
        List<List<String>> records = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(url))) {
            String line;

            while ((line = br.readLine()) != null) {
                String[] values = line.split(COMMA_DELIMITER);
                records.add(Arrays.asList(values));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return records;
    }

    static List<List<Double>> stringToDouble(List<List<String>> records) {
        List<List<Double>> recordsDouble = new ArrayList<>();

        for (int i = 0; i < records.size(); i++) {
            ArrayList<Double> temp = new ArrayList<Double>();

            for (int j = 0; j < records.get(i).size(); j++) {
                temp.add(Double.valueOf(records.get(i).get(j)));
            }

            recordsDouble.add(temp);
        }

        return recordsDouble;
    }

    static double knnWithHoldout(List<List<Double>> recordsDouble, int k, double dataTrainRatio) {
        List<List<Double>> dataTraining = new ArrayList<>();
        List<List<Double>> dataTesting = new ArrayList<>();

        holdoutSplit(dataTraining, dataTesting, recordsDouble, dataTrainRatio);
        dataTraining = dataTrainingNormalize(dataTraining);
        dataTesting = dataTestingNormalize(dataTesting);

        List<List<Double>> dataResult = klasifikasiKnn(dataTraining, dataTesting, k);

        // cari error
        int errorCount = 0;
        int dataCount = dataTesting.size();
        int labelIndex = dataTesting.get(0).size() - 1;

        for (int i = 0; i < dataResult.size(); i++) {
            if (!dataResult.get(i).get(labelIndex).equals(dataTesting.get(i).get(labelIndex))) {
                errorCount++;
            }
        }

        double errorRate = (double) errorCount / (double) dataCount * 100;

        return errorRate;
    }

    static void holdoutSplit(List<List<Double>> dataTraining, List<List<Double>> dataTesting,
            List<List<Double>> recordsDouble, double dataTrainRatio) {

        List<List<Double>> flagArray = buildFlagArray(recordsDouble); // build array yg isinya label, jumlah, flag

        for (List<Double> line : recordsDouble) {
            double label = line.get(line.size() - 1);
            int findLabelByIndex = -1;

            for (int i = 0; i < flagArray.size(); i++) {
                double labelInFlagArray = flagArray.get(i).get(0); // ambil label dari flagArray

                if (label == labelInFlagArray) {
                    findLabelByIndex = i;
                    break;
                }
            }

            if (findLabelByIndex != -1) {
                int flagIndex = flagArray.get(findLabelByIndex).size() - 1;
                double getLabelFlag = flagArray.get(findLabelByIndex).get(flagIndex);

                double getLabelTotal = flagArray.get(findLabelByIndex).get(1);

                if (getLabelFlag < getLabelTotal * dataTrainRatio) {
                    dataTraining.add(line);
                } else {
                    dataTesting.add(line);
                }

                // tambah flag
                flagArray.get(findLabelByIndex).set(flagIndex, getLabelFlag + 1);
            }
        }

        // Tes print data training
        // int i = 1;
        // System.out.println("Data Training");
        // for (List<Double> lineData : dataTraining) {
        // System.out.print(i + " => ");
        // for (Double cellData : lineData) {
        // System.out.print(cellData + "\t");
        // }
        // i++;
        // System.out.println();
        // }

        // Tes print data testing
        // int i = 1;
        // System.out.println("Data Testing");
        // for (List<Double> lineData : dataTesting) {
        // System.out.print(i + " => ");
        // for (Double cellData : lineData) {
        // System.out.print(cellData + "\t");
        // }
        // i++;
        // System.out.println();
        // }
    }

    static double knnWithKFold(List<List<Double>> recordsDouble, int k, double dataTestRatio) {
        List<List<Double>> dataTraining = new ArrayList<>();
        List<List<Double>> dataTesting = new ArrayList<>();

        double meanError = 0;
        int steps = (int) (1 / dataTestRatio);

        for (int i = 0; i < steps; i++) {
            kFoldSplit(dataTraining, dataTesting, recordsDouble, dataTestRatio, i);

            dataTraining = dataTrainingNormalize(dataTraining);
            dataTesting = dataTestingNormalize(dataTesting);

            List<List<Double>> dataResult = klasifikasiKnn(dataTraining, dataTesting, k);

            // cari error
            int errorCount = 0;
            int dataCount = dataTesting.size();
            int labelIndex = dataTesting.get(0).size() - 1;

            for (int j = 0; j < dataResult.size(); j++) {
                if (!dataResult.get(j).get(labelIndex).equals(dataTesting.get(j).get(labelIndex))) {
                    errorCount++;
                }
            }

            double errorRate = (double) errorCount / (double) dataCount * 100;
            meanError += errorRate;
        }
        meanError /= steps;

        return meanError;
    }

    static void kFoldSplit(List<List<Double>> dataTraining, List<List<Double>> dataTesting,
            List<List<Double>> recordsDouble, double dataTestRatio, int step) {
        dataTraining.clear();
        dataTesting.clear();

        List<List<Double>> flagArray = buildFlagArray(recordsDouble); // build array yg isinya label, jumlah, flag

        for (int i = 0; i < recordsDouble.size(); i++) {
            int findLabelByIndex = -1;

            for (int j = 0; j < flagArray.size(); j++) {
                double recordsDoubleLabel = recordsDouble.get(i).get(recordsDouble.get(i).size() - 1);
                double flagArrayLabel = flagArray.get(j).get(flagArray.get(j).size() - flagArray.get(j).size());

                if (recordsDoubleLabel == flagArrayLabel) {
                    findLabelByIndex = j;
                    break;
                }
            }

            if (findLabelByIndex != -1) {
                double flagArrayFlag = flagArray.get(findLabelByIndex).get(2);
                double flagArrayJumlah = flagArray.get(findLabelByIndex).get(1);

                // System.out.print(flagArrayFlag + " < " + dataTestRatio + " * " +
                // flagArrayJumlah + " + (" + step + " * "
                // + dataTestRatio + " * " + flagArrayJumlah + ") && " + flagArrayFlag + " > " +
                // step + " * "
                // + dataTestRatio + " * " + flagArrayJumlah + " ");
                // System.out.println(
                // flagArrayFlag < dataTestRatio * flagArrayJumlah + (step * dataTestRatio *
                // flagArrayJumlah)
                // && flagArrayFlag >= step * dataTestRatio * flagArrayJumlah);
                if (flagArrayFlag < dataTestRatio * flagArrayJumlah + (step * dataTestRatio * flagArrayJumlah)
                        && flagArrayFlag >= step * dataTestRatio * flagArrayJumlah) {
                    dataTesting.add(recordsDouble.get(i));
                } else {
                    dataTraining.add(recordsDouble.get(i));
                }
                flagArrayFlag += 1;
                flagArray.get(findLabelByIndex).set(2, flagArrayFlag);
            }
        }

        // Tes print data training
        // int i = 1;
        // System.out.println("Step: " + step);
        // System.out.println("Data Training");
        // for (List<Double> lineData : dataTraining) {
        // System.out.print(i + " => ");
        // for (Double cellData : lineData) {
        // System.out.print(cellData + "\t");
        // }
        // i++;
        // System.out.println();
        // }

        // Tes print data testing
        // int i = 1;
        // System.out.println("Step: " + step);
        // System.out.println("Data Testing");
        // for (List<Double> lineData : dataTesting) {
        // System.out.print(i + " => ");
        // for (Double cellData : lineData) {
        // System.out.print(cellData + "\t");
        // }
        // i++;
        // System.out.println();
        // }
    }

    static double knnWithLOO(List<List<Double>> recordsDouble, int k) {
        List<List<Double>> dataTraining = new ArrayList<>();
        List<List<Double>> dataTesting = new ArrayList<>();

        double meanError = 0;
        int steps = recordsDouble.size();

        for (int i = 0; i < steps; i++) {
            looSplit(dataTraining, dataTesting, recordsDouble, i);

            dataTraining = dataTrainingNormalize(dataTraining);
            dataTesting = dataTestingNormalize(dataTesting);

            List<List<Double>> dataResult = klasifikasiKnn(dataTraining, dataTesting, k);

            // cari error
            int errorCount = 0;
            int dataCount = dataTesting.size();
            int labelIndex = dataTesting.get(0).size() - 1;

            for (int j = 0; j < dataResult.size(); j++) {
                if (!dataResult.get(j).get(labelIndex).equals(dataTesting.get(j).get(labelIndex))) {
                    errorCount++;
                }
            }

            double errorRate = (double) errorCount / (double) dataCount * 100;
            meanError += errorRate;
        }
        meanError /= steps;

        return meanError;
    }

    static void looSplit(List<List<Double>> dataTraining, List<List<Double>> dataTesting,
            List<List<Double>> recordsDouble, int step) {
        dataTraining.clear();
        dataTesting.clear();

        List<List<Double>> flagArray = buildFlagArray(recordsDouble); // build array yg isinya label, jumlah, flag

        for (int i = 0; i < recordsDouble.size(); i++) {
            int findLabelByIndex = -1;

            for (int j = 0; j < flagArray.size(); j++) {
                double recordsDoubleLabel = recordsDouble.get(i).get(recordsDouble.get(i).size() - 1);
                double flagArrayLabel = flagArray.get(j).get(flagArray.get(j).size() - flagArray.get(j).size());

                if (recordsDoubleLabel == flagArrayLabel) {
                    findLabelByIndex = j;
                    break;
                }
            }

            if (findLabelByIndex != -1) {
                double flagArrayFlag = flagArray.get(findLabelByIndex).get(2);

                // System.out.print(flagArrayFlag + " < " + dataTestRatio + " * " +
                // flagArrayJumlah + " + (" + step + " * "
                // + dataTestRatio + " * " + flagArrayJumlah + ") && " + flagArrayFlag + " > " +
                // step + " * "
                // + dataTestRatio + " * " + flagArrayJumlah + " ");
                // System.out.println(
                // flagArrayFlag < dataTestRatio * flagArrayJumlah + (step * dataTestRatio *
                // flagArrayJumlah)
                // && flagArrayFlag >= step * dataTestRatio * flagArrayJumlah);
                if (i == step) {
                    dataTesting.add(recordsDouble.get(i));
                } else {
                    dataTraining.add(recordsDouble.get(i));
                }
                flagArrayFlag += 1;
                flagArray.get(findLabelByIndex).set(2, flagArrayFlag);
            }
        }

        // Tes print data training
        // int i = 1;
        // System.out.println("Step: " + step);
        // System.out.println("Data Training");
        // for (List<Double> lineData : dataTraining) {
        // System.out.print(i + " => ");
        // for (Double cellData : lineData) {
        // System.out.print(cellData + "\t");
        // }
        // i++;
        // System.out.println();
        // }

        // Tes print data testing
        // int i = 1;
        // System.out.println("Step: " + step);
        // System.out.println("Data Testing");
        // for (List<Double> lineData : dataTesting) {
        // System.out.print(i + " => ");
        // for (Double cellData : lineData) {
        // System.out.print(cellData + "\t");
        // }
        // i++;
        // System.out.println();
        // }
    }

    // build array yg isinya label, jumlah, dan flag
    static List<List<Double>> buildFlagArray(List<List<Double>> recordsInt) {
        List<List<Double>> flagArray = new ArrayList<>(); // label, jumlah, flag

        // akses semua baris data
        for (List<Double> line : recordsInt) {
            double label = line.get(line.size() - 1); // ambil label dari data
            ArrayList<Double> flagArrayLine = new ArrayList<Double>();

            // perulangan flagArray buat dapetin banyak label dan kuantitas tiap label
            if (flagArray.isEmpty()) { // first time setup
                flagArrayLine.add(label);
                flagArrayLine.add(1.0);
                flagArrayLine.add(0.0);

                flagArray.add(flagArrayLine);
            } else { // kalo bukan first time
                int getExistingLabelIndex = -1;

                for (int i = 0; i < flagArray.size(); i++) {
                    double labelInFlagArray = flagArray.get(i).get(0); // ambil label dari flagArray

                    if (label == labelInFlagArray) {
                        getExistingLabelIndex = i;
                        break;
                    }
                }

                if (getExistingLabelIndex == -1) { // jika ditemukan label baru
                    flagArrayLine.add(label);
                    flagArrayLine.add(1.0);
                    flagArrayLine.add(0.0);

                    flagArray.add(flagArrayLine);
                    // System.out.println(label);
                } else { // jika labelnya udah ada
                    double getLabelTotal = flagArray.get(getExistingLabelIndex).get(1);
                    flagArray.get(getExistingLabelIndex).set(1, getLabelTotal + 1);
                }
            }
        }

        // Tes print isi flagArray
        // int i = 1;
        // for (List<Double> lineData : flagArray) {
        // System.out.print(i + " => ");
        // for (Double cellData : lineData) {
        // System.out.print(cellData + " | ");
        // }
        // i++;
        // System.out.println();
        // }

        return flagArray;
    }

    static List<List<Double>> dataTrainingNormalize(List<List<Double>> dataTraining) {
        List<List<Double>> newData = new ArrayList<>();

        // cari mean (rata")
        List<Double> average = averagePerColumn(dataTraining);
        List<Double> std = stdPerColumn(dataTraining, average);

        // Tes print average per column
        // for(Double value : average) {
        // System.out.println(value);
        // }

        // Tes print average per column
        // for(Double value : std) {
        // System.out.println(value);
        // }

        for (int i = 0; i < dataTraining.size(); i++) {
            List<Double> temp = new ArrayList<>();

            for (int j = 0; j < dataTraining.get(i).size(); j++) {
                if (j == dataTraining.get(i).size() - 1) {
                    temp.add(dataTraining.get(i).get(j));
                } else {
                    temp.add((dataTraining.get(i).get(j) - average.get(j)) / std.get(j));
                }
            }
            newData.add(temp);
        }

        // int i = 0;
        // for(List<Double> line : newData) {
        // System.out.print(i + " => " );
        // for(Double cell : line) {
        // System.out.print(cell + "\t");
        // }
        // System.out.println();
        // i++;
        // }

        return newData;
    }

    static List<List<Double>> dataTestingNormalize(List<List<Double>> dataTesting) {
        List<List<Double>> newData = new ArrayList<>();

        // cari mean (rata")
        List<Double> average = averagePerColumn(dataTesting);
        List<Double> std = stdPerColumn(dataTesting, average);

        // Tes print average per column
        // for(Double value : average) {
        // System.out.println(value);
        // }

        // Tes print average per column
        // for(Double value : std) {
        // System.out.println(value);
        // }

        for (int i = 0; i < dataTesting.size(); i++) {
            List<Double> temp = new ArrayList<>();

            for (int j = 0; j < dataTesting.get(i).size(); j++) {
                if (j == dataTesting.get(i).size() - 1) {
                    temp.add(dataTesting.get(i).get(j));
                } else {
                    temp.add((dataTesting.get(i).get(j) - average.get(j)) / std.get(j));
                }
            }
            newData.add(temp);
        }

        // int i = 0;
        // for(List<Double> line : newData) {
        // System.out.print(i + " => " );
        // for(Double cell : line) {
        // System.out.print(cell + "\t");
        // }
        // System.out.println();
        // i++;
        // }

        return newData;
    }

    static List<Double> averagePerColumn(List<List<Double>> data) {
        List<Double> average = new ArrayList<>();

        int columnLength = data.get(0).size();
        for (int i = 0; i < columnLength - 1; i++) {
            double averageTemp = 0;
            double sumTemp = 0;

            for (int j = 0; j < data.size(); j++) {
                sumTemp += data.get(j).get(i);
            }

            averageTemp = sumTemp / data.size();
            average.add(averageTemp);
        }

        return average;
    }

    static List<Double> stdPerColumn(List<List<Double>> data, List<Double> average) {
        List<Double> std = new ArrayList<>();

        int columnLength = data.get(0).size();
        for (int i = 0; i < columnLength - 1; i++) {
            double stdTemp = 0;

            double averageInThisColumn = average.get(i);

            for (int j = 0; j < data.size(); j++) {
                stdTemp += Math.pow(data.get(j).get(i) - averageInThisColumn, 2);
            }

            stdTemp = Math.sqrt(stdTemp / data.size());
            std.add(stdTemp);
        }

        return std;
    }

    static List<List<Double>> klasifikasiKnn(List<List<Double>> dataTraining, List<List<Double>> dataTesting, int k) {
        List<List<Double>> testingResult = new ArrayList<>(); // sama dengan data testing namun labelnya diisi hasil
                                                              // klasifikasi

        // System.out.println("Print data testing di awal fungsi klasifikasi");
        // for(List<Double> row : dataTesting) {
        // for(Double col : row) {
        // System.out.print(col + " ");
        // }
        // System.out.println();
        // }
        // System.out.println("Print data testing di perulangan");
        for (int i = 0; i < dataTesting.size(); i++) {
            List<List<Double>> penampungDistanceLabel = new ArrayList<>();

            for (int j = 0; j < dataTraining.size(); j++) {
                List<Double> temp = new ArrayList<>();

                double trainingPoint[] = new double[dataTraining.get(j).size() - 1];
                double testingPoint[] = new double[dataTesting.get(i).size() - 1];
                for (int a = 0; a < dataTraining.get(j).size() - 1; a++) {
                    trainingPoint[a] = dataTraining.get(j).get(a);
                    testingPoint[a] = dataTesting.get(i).get(a);
                }
                temp.add(getDistance(trainingPoint, testingPoint));

                double label = dataTraining.get(j).get(dataTraining.get(j).size() - 1);
                temp.add(label);

                // System.out.println(temp + " ");
                penampungDistanceLabel.add(temp);
            }
            // System.out.println("");

            penampungDistanceLabel = sortDoubleList(penampungDistanceLabel);

            // tes print apakah sudah terurut atau belom [HASIL: SUDAH]
            // int l = 1;
            // System.out.println("Data ke-" + i);
            // for(List<Double> line : penampungDistanceLabel) {
            // for(Double cell : line) {
            // System.out.print(cell + " ");
            // }
            // l++;
            // System.out.println();
            // }
            // System.out.println();

            List<List<Double>> kFiltered = new ArrayList<>();
            for (int a = 0; a < k; a++) {
                kFiltered.add(penampungDistanceLabel.get(a));
            }

            // tes print apakah sudah kefilter sebanyak k [HASIL: SUDAH]
            // for(List<Double> line : kFiltered) {
            // for(Double cell : line) {
            // System.out.print(cell + " ");
            // }
            // System.out.println();
            // }
            // System.out.println();

            // List<Double> labelResult = findLabelResult(kFiltered);
            double labelResult = findLabelResult(kFiltered);
            // System.out.println(labelResult);

            List<Double> lineTempDouble = new ArrayList<>();

            for (Double col : dataTesting.get(i)) {
                lineTempDouble.add(col);
            }

            // for(Double col : dataTesting.get(i)) {
            // System.out.print(col + " ");
            // }
            // System.out.println("");

            for (int a = 0; a < lineTempDouble.size(); a++) {
                if (a == lineTempDouble.size() - 1) {
                    lineTempDouble.set(a, labelResult);
                }
            }

            testingResult.add(lineTempDouble);
        }

        return testingResult;

        // Tes print untuk mengecek isi hasil klasifikasi
        // System.out.println("Testing result");
        // for(List<Double> line : testingResult) {
        // for(Double cell : line) {
        // System.out.print(cell + " ");
        // }
        // System.out.println();
        // }

        // System.out.println("Print data testing di akhir fungsi klasifikasi");
        // for(List<Double> row : dataTesting) {
        // for(Double col : row) {
        // System.out.print(col + " ");
        // }
        // System.out.println();
        // }

        // Hitung error
        // int errorCount = 0;
        // int dataCount = dataTesting.size();
        // int labelIndex = dataTesting.get(0).size() - 1;

        // print perbandingan data testing dan data hasil prediksi
        // for (int i = 0; i < testingResult.size(); i++) {
        // // System.out.println(testingResult.get(i).get(labelIndex) + " : " +
        // // dataTesting.get(i).get(labelIndex));
        // if
        // (!testingResult.get(i).get(labelIndex).equals(dataTesting.get(i).get(labelIndex)))
        // {
        // errorCount++;
        // }
        // }
        // System.out.println();

        // double errorRate = (double) errorCount / (double) dataCount * 100;

        // // print error
        // System.out.println("k = " + k);
        // System.out.println("Error rate: " + errorRate + "%");
        // System.out.println("Error count: " + errorCount);
        // System.out.println();
    }

    static double getDistance(double point1[], double point2[]) {
        double sum = 0;

        for (int i = 0; i < point1.length; i++) {
            sum += Math.pow(point1[i] - point2[i], 2);
        }

        return Math.sqrt(sum);
    }

    static List<List<Double>> sortDoubleList(List<List<Double>> list) {
        for (int i = 0; i < list.size() - 1; i++) {
            for (int j = 0; j < list.size() - i - 1; j++) {
                if (list.get(j).get(0) > list.get(j + 1).get(0)) {
                    List<Double> temp = list.get(j);
                    list.set(j, list.get(j + 1));
                    list.set(j + 1, temp);
                }
            }
        }

        return list;
    }

    static double findLabelResult(List<List<Double>> kFiltered) {
        List<List<Double>> penampungQtyLabelDistance = new ArrayList<>(); // qty, distance, label

        for (int i = 0; i < kFiltered.size(); i++) {
            List<Double> temp = new ArrayList<>();

            if (i == 0) {
                temp.add((double) 1);
                temp.add(kFiltered.get(i).get(0));
                temp.add(kFiltered.get(i).get(1));

                penampungQtyLabelDistance.add(temp);
            } else {
                int getExistingLabelIndex = -1;

                for (int j = 0; j < penampungQtyLabelDistance.size(); j++) {
                    if (penampungQtyLabelDistance.get(j).get(2).equals(kFiltered.get(i).get(1))) { // kfiltered:
                                                                                                   // distance, label,
                                                                                                   // penampung...L:
                                                                                                   // qty, distance,
                                                                                                   // label
                        getExistingLabelIndex = j;

                        break;
                    }
                }

                if (getExistingLabelIndex == -1) {
                    temp.add((double) 1);
                    temp.add(kFiltered.get(i).get(0));
                    temp.add(kFiltered.get(i).get(1));

                    penampungQtyLabelDistance.add(temp);
                } else {
                    double qtyTemp = penampungQtyLabelDistance.get(getExistingLabelIndex).get(0);
                    penampungQtyLabelDistance.get(getExistingLabelIndex).set(0, qtyTemp + 1);
                }
            }
        }

        // Tes untuk mengecek isi penampungQtyLabelDistance apakah sudah berisi qty
        // distance label
        // for(List<Double> line : penampungQtyLabelDistance) {
        // for(Double cell : line) {
        // System.out.print(cell + " ");
        // }
        // System.out.println();
        // }

        int labelWithMaxQtyIndex = 0;

        for (int i = 0; i < penampungQtyLabelDistance.size(); i++) {
            if (i == 0) {
                labelWithMaxQtyIndex = i;
            } else {
                if (penampungQtyLabelDistance.get(labelWithMaxQtyIndex).get(0) < penampungQtyLabelDistance.get(i)
                        .get(0)) {
                    labelWithMaxQtyIndex = i;
                }
            }
        }

        // Tes print mengecek isi penampungQtyLabelDistance dan labelWithMaxQtyIndex
        // untuk memastikan
        // for(List<Double> line : penampungQtyLabelDistance) {
        // for(Double cell : line) {
        // System.out.print(cell + " ");
        // }
        // System.out.println();
        // }

        // System.out.println("labelWithMaxQtyIndex: " + labelWithMaxQtyIndex);

        int maxQuantityFound = 0;
        for (int i = 0; i < penampungQtyLabelDistance.size(); i++) {
            if (penampungQtyLabelDistance.get(i).get(0)
                    .equals(penampungQtyLabelDistance.get(labelWithMaxQtyIndex).get(0))) {
                maxQuantityFound++;
            }
        }

        if (maxQuantityFound == 1) {
            return penampungQtyLabelDistance.get(labelWithMaxQtyIndex).get(2);
        } else {
            int maxQuantityShortestDistanceIndex = 0;
            for (int i = 0; i < penampungQtyLabelDistance.size(); i++) {
                if (penampungQtyLabelDistance.get(i).get(1) < penampungQtyLabelDistance
                        .get(maxQuantityShortestDistanceIndex).get(1)) {
                    maxQuantityShortestDistanceIndex = i;
                }
            }
            return penampungQtyLabelDistance.get(maxQuantityShortestDistanceIndex).get(2);
            // return kFiltered.get(0).get(1);
        }
        // return kFiltered.get(0).get(1);
        // return penampungQtyLabelDistance.get(labelWithMaxQtyIndex).get(2);
    }
}